//
//  ViewController.swift
//  DelegateExample
//
//  Created by  Masoud Moharrami on 3/7/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

class ViewController: UIViewController, SelectionPageDelegate {
    

    @IBOutlet weak var cardTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var expirationDate: UITextField!
    @IBOutlet weak var verificationTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var payButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //keyboard handling
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: .UIKeyboardWillHide, object: nil)
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        payButton.layer.cornerRadius = payButton.bounds.height/2
        payButton.layer.shadowRadius = 6
        payButton.layer.shadowOpacity = 0.4
        payButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        payButton.layer.masksToBounds = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Choose Card Delegation
    func didTapChoise(cardNumber: String, expirationDate: String) {
        cardTextField.text = cardNumber
        self.expirationDate.text = expirationDate
    }
    
    // MARK: - IBActions
    @IBAction func payClicked(_ sender: Any) {
        if cardTextField.text != "" && passTextField.text != "" && expirationDate.text != "" && verificationTextField.text != "" {
            
            let alertController = UIAlertController(title: "Successful", message: "Your payment seccessfully done.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
            
        }else{
            let alertController = UIAlertController(title: "Problem", message: "Fill all the fields", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func chooseCardClicked(_ sender: Any) {
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "SelectionPage") as? ViewController2
        selectionVC?.selectionDelegate = self
        present(selectionVC!, animated: true, completion: nil)
    }
    // MARK: - Keyboard handleing
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
   @objc func handleKeyboardNotification(notification: Notification){
        if let userInfo = notification.userInfo {
            
            let keyboardFrame: NSValue = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            let isKeyboardShowing = notification.name == .UIKeyboardWillShow
            self.view.frame.origin.y = isKeyboardShowing ? -keyboardHeight: 0
            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
}

