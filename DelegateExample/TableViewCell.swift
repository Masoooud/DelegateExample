//
//  TableViewCell.swift
//  DelegateExample
//
//  Created by  Masoud Moharrami on 3/7/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell{
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewFront: UIView!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var expirationDate: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBack.layer.cornerRadius = 8
        viewFront.layer.cornerRadius = 5
        
        viewBack.layer.masksToBounds = false
        
        viewBack.layer.shadowColor = UIColor.init(red: 0, green: 122/255, blue: 1, alpha: 1).cgColor
        viewBack.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewBack.layer.shadowOpacity = 0.4
        viewBack.layer.shadowRadius = 6
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    


}
