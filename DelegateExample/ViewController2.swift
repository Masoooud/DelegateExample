//
//  ViewController2.swift
//  DelegateExample
//
//  Created by  Masoud Moharrami on 3/7/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

protocol SelectionPageDelegate {
    func didTapChoise(cardNumber: String, expirationDate: String)
}

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var cards = [Card]()
    
    var selectionDelegate: SelectionPageDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        addMockCards()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BankCard", for: indexPath) as? TableViewCell{
            cell.bankName.text = cards[row].bankName
            cell.expirationDate.text = cards[row].expirationDate
            cell.cardNumber.text = cards[row].cardNumber
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 182
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectionDelegate.didTapChoise(cardNumber: cards[indexPath.row].cardNumber, expirationDate: cards[indexPath.row].expirationDate)
        dismiss(animated: true, completion: nil)
    }

    func addMockCards(){
        let card1 = Card(bankName: "Tejarat", cardNumber: "xxxx-xxxx-xxxx-0097", expirationDate: "11/99")
        cards.append(card1)
        
        let card2 = Card(bankName: "Saman", cardNumber: "xxxx-xxxx-xxxx-0245", expirationDate: "11/98")
        cards.append(card2)
        
        let card3 = Card(bankName: "Eghtesad", cardNumber: "xxxx-xxxx-xxxx-6453", expirationDate: "11/97")
        cards.append(card3)
        
        let card4 = Card(bankName: "Melli", cardNumber: "xxxx-xxxx-xxxx-0430", expirationDate: "11/99")
        cards.append(card4)
        
        let card5 = Card(bankName: "Mellat", cardNumber: "xxxx-xxxx-xxxx-0040", expirationDate: "11/00")
        cards.append(card5)
    }
}
struct Card{
    var bankName: String!
    var cardNumber: String!
    var expirationDate: String!
}
